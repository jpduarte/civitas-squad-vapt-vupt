import React from "react";
import Routes from "./src/screens";
import RegisterSuccess from "./src/screens/RegisterPages/RegisterSuccess";

export default function App() {
  return <Routes />;
}
