import { StyleSheet } from "react-native";
import styled from "styled-components/native";

export const ImageBox = styled.View`
  width: 150px;
  height: 150px;
  margin-top: 5%;
  border: 1px solid #000;
  border-radius: 5;
  align-self: center;
`;

export const NavigationView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const InputBoxText = StyleSheet.create({
  inputBox: {
    borderColor: "#000",
    borderWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    marginBottom: 5,
    borderRadius: 10,
  },

  inputBoxDescription: {
    paddingVertical: 50,
    borderColor: "#000",
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 5,
    borderRadius: 10,
  },

  text: {
    color: "#000",
    fontSize: 18,
    paddingLeft: 10,
  },

  button: {
    marginTop: 15,
  },

  optionBox: {
    borderColor: "#000",
    width: "50%",
    alignItems: "center",
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 40,
    borderRadius: 10,
  },
});
