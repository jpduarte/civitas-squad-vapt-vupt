import React from "react";
import Header from "../../../components/Header";
import NavigationBar from "../../../components/NavigationBar/Default";
import { Image, TextInput, Text, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { onChange } from "react-native-reanimated";
import { HomeView, FeedView, SalesTitle } from "../../style";
import { ImageBox, InputBoxText } from "../style";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useForm, Controller } from "react-hook-form";
import { TextInputMask } from "react-native-masked-text";
import PickerItem from "../../../components/PickerItem";
import Button from "../../../components/Button";

export default function CreateSale() {
  const { control, handleSubmit } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  interface FormData {
    title: string;
    description: string;
    banner_photo: string;
    price: string;
    type: string;
  }

  const navigation = useNavigation();

  return (
    <HomeView>
      <Header
        actionLogout={() => navigation.navigate("Login")}
        actionProfile={() => navigation.navigate("Profile")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <FeedView>
          <SalesTitle>Anunciar Venda</SalesTitle>
          <ImageBox>
            <Text>add picture here</Text>
          </ImageBox>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Título"
                  maxLength={25}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "O título é obrigatório",
              }}
              name="title"
              defaultValue=""
            />
          </View>
          <View style={InputBoxText.inputBoxDescription}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Descrição"
                  maxLength={100}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              name="description"
              defaultValue=""
            />
          </View>

          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInputMask
                  placeholder="Preço"
                  maxLength={10}
                  type={"datetime"}
                  options={{
                    format: "$ 999,99",
                  }}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              name="price"
              defaultValue=""
            />
          </View>
          <View style={InputBoxText.optionBox}>
            <PickerItem
              optionLabelOne={"Produto"}
              optionLabelTwo={"Serviço"}
              optionValueOne={"product"}
              optionValueTwo={"service"}
            />
          </View>
          <TouchableOpacity onPress={() => navigation.navigate("HomeServices")}>
            <Button value={"Confirmar alterações"} />
          </TouchableOpacity>
        </FeedView>
      </ScrollView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionFavorite={() => {
          navigation.navigate("UserFavoriteServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
        actionCart={() => {
          navigation.navigate("Cart");
        }}
      />
    </HomeView>
  );
}
