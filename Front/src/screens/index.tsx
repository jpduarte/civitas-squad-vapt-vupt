import React from "react";
import Register from "./RegisterPages/Register/index";
import RegisterSuccess from "./RegisterPages/RegisterSuccess/index";
import HomeProducts from "./Home/HomeProducts/index";
import CreateSale from "./ManageSale/CreateSale/index";
import EditSale from "./ManageSale/EditSale/index";
import Profile from "./Profile/index";
import UserFavoriteProducts from "./UserFavorite/UserFavoriteProducts/index";
import HomeServices from "./Home/HomeServices/index";
import UserFavoriteServices from "./UserFavorite/UserFavoriteServices/index";
import UserServices from "./UserSales/UserServices/index";
import UserProducts from "./UserSales/UserProducts/index";
import Cart from "./Cart/index";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "./Login";
import RecoverPassword from "./RecoverPassword";
import ProductPage from "./ProductsServices/ProductPage/Index";
import ServicePage from "./ProductsServices/ServicePage";

const { Navigator, Screen } = createStackNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Screen name="Login" component={Login} />
        <Screen name="RecoverPassword" component={RecoverPassword} />
        <Screen name="Register" component={Register} />
        <Screen name="RegisterSuccess" component={RegisterSuccess} />
        <Screen name="HomeServices" component={HomeServices} />
        <Screen name="HomeProducts" component={HomeProducts} />
        <Screen name="ProductPage" component={ProductPage} />
        <Screen name="ServicePage" component={ServicePage} />
        <Screen name="CreateSale" component={CreateSale} />
        <Screen name="EditSale" component={EditSale} />
        <Screen name="Profile" component={Profile} />
        <Screen name="UserFavoriteServices" component={UserFavoriteServices} />
        <Screen name="UserFavoriteProducts" component={UserFavoriteProducts} />
        <Screen name="UserServices" component={UserServices} />
        <Screen name="UserProducts" component={UserProducts} />
        <Screen name="Cart" component={Cart} />
      </Navigator>
    </NavigationContainer>
  );
}
