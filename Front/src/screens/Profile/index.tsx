import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import {
  Text,
  Image,
  TouchableOpacity,
  View,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";

import { useForm, Controller } from "react-hook-form";
import { onChange } from "react-native-reanimated";
import { TextInputMask } from "react-native-masked-text";

import Button from "../../components/Button";

import { InputBoxText } from "../ManageSale/style";

import { HeaderApp, HeaderText, ImageCircle, PictureView } from "./style";
import { FeedView, HomeView, NavigationTextSelected } from "../style";
import NavigationBar from "../../components/NavigationBar/Default";
import { useNavigation } from "@react-navigation/native";

export default function Profile() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };
  interface FormData {
    name: string;
    username: string;
    email: string;
    password: string;
    birthDate: string;
    city: string;
    state: string;
    neighborhood: string;
    phoneNumber: number;
  }

  const navigation = useNavigation();

  return (
    <HomeView>
      <HeaderApp>
        <HeaderText>Olá, Lucicreia!</HeaderText>
        <TouchableOpacity onPress={() => {}}>
          <Image source={require("../../../assets/logout.png")} />
        </TouchableOpacity>
      </HeaderApp>
      <FeedView>
        <KeyboardAvoidingView
          style={{ flex: 1, flexDirection: "column", justifyContent: "center" }}
          behavior="padding"
          enabled
          keyboardVerticalOffset={100}
        >
          <ScrollView showsVerticalScrollIndicator={false}>
            <PictureView>
              <ImageCircle>
                <Image
                  source={require("../../../assets/profile-pic-default.png")}
                />
              </ImageCircle>
              <TouchableOpacity>
                <Image source={require("../../../assets/edit-icon.png")} />
              </TouchableOpacity>
            </PictureView>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Nome"
                    maxLength={25}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O nome é obrigatório",
                }}
                name="name"
                defaultValue=""
              />
              {errors.name && (
                <Text style={{ color: "red" }}>{errors.name.message}</Text>
              )}
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Nome de usuário"
                    maxLength={15}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O nome de usuário é obrigatório",
                }}
                name="username"
                defaultValue=""
              />
              {errors.username && (
                <Text style={{ color: "red" }}>{errors.username.message}</Text>
              )}
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="E-mail"
                    maxLength={20}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O e-mail é obrigatório",
                  pattern: {
                    value: /^\S+@\S+$/i,
                    message: "Formato inválido",
                  },
                }}
                name="email"
                defaultValue=""
              />
              {errors.email && (
                <Text style={{ color: "red" }}>{errors.email.message}</Text>
              )}
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Senha"
                    maxLength={20}
                    secureTextEntry
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "A senha é obrigatória",
                }}
                name="password"
                defaultValue=""
              />
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInputMask
                    placeholder="Data de nascimento"
                    type={"datetime"}
                    options={{
                      format: "DD/MM/YYYY",
                    }}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                name="birthDate"
                defaultValue=""
              />
            </View>

            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Cidade"
                    maxLength={25}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "A cidade é obrigatória",
                }}
                name="city"
                defaultValue=""
              />
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Estado"
                    maxLength={20}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O estado é obrigatório",
                }}
                name="state"
                defaultValue=""
              />
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Bairro"
                    maxLength={30}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O bairro é obrigatório",
                }}
                name="neighborhood"
                defaultValue=""
              />
            </View>
            <View style={InputBoxText.inputBox}>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInputMask
                    placeholder="N° de telefone"
                    type={"datetime"}
                    options={{
                      format: "(99) 99999-9999",
                    }}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                name="cell_phone"
                defaultValue=""
              />
            </View>
            <TouchableOpacity
              style={InputBoxText.button}
              onPress={() => navigation.navigate("HomeServices")}
            >
              <Button value="Confirmar alterações" />
            </TouchableOpacity>
          </ScrollView>
        </KeyboardAvoidingView>
      </FeedView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionFavorite={() => {
          navigation.navigate("UserFavoriteServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
        actionCart={() => {
          navigation.navigate("Cart");
        }}
      />
    </HomeView>
  );
}
