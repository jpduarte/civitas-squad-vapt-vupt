import React from "react";
import styled from "styled-components/native";

export const HeaderApp = styled.View`
  background-color: #d9fde3;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10% 10% 5% 5%;
  justify-content: space-between;
`;

export const HeaderText = styled.Text`
  margin-left: 20;
  font-size: 18;
  color: #ff5894;
`;

export const ImageCircle = styled.View`
  width: 150px;
  height: 150px;
  border-radius: 100;
`;

export const PictureView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
