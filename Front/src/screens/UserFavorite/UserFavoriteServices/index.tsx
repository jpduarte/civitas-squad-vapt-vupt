import React from "react";
import Header from "../../../components/Header";
import NavigationBar from "../../../components/NavigationBar/Favorite";
import { Image, TextInput } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { onChange } from "react-native-reanimated";
import {
  HomeView,
  FeedView,
  NavigationView,
  ItemsView,
  NavigationText,
  FeedImage,
  NavigationTextSelected,
  InputView,
  InputStyle,
  SalesContainer,
  SalesTitle,
} from "../../style";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useForm, Controller } from "react-hook-form";

export default function UserFavoriteServices() {
  const { control, handleSubmit } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  interface FormData {
    filter: string;
  }

  const navigation = useNavigation();

  return (
    <HomeView>
      <Header
        actionProfile={() => navigation.navigate("Profile")}
        actionLogout={() => navigation.navigate("Login")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <FeedView>
          <SalesContainer>
            <SalesTitle>Favoritos</SalesTitle>
          </SalesContainer>
          <NavigationView>
            <TouchableOpacity>
              <NavigationTextSelected>Serviços</NavigationTextSelected>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate("UserFavoriteProducts")}
            >
              <NavigationText>Produtos</NavigationText>
            </TouchableOpacity>
          </NavigationView>
          <InputView>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Pesquisar"
                  maxLength={20}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputStyle.inputBox}
                />
              )}
              name="filter"
            />
            <TouchableOpacity onPress={(handleSubmit(onSubmit), () => {})}>
              <Image source={require("../../../../assets/filter-icon.png")} />
            </TouchableOpacity>
          </InputView>
          <ItemsView>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={FeedImage.image}
                source={require("../../../../assets/default-item.png")}
              />
            </TouchableOpacity>
          </ItemsView>
        </FeedView>
      </ScrollView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
        actionCart={() => {
          navigation.navigate("Cart");
        }}
      />
    </HomeView>
  );
}
