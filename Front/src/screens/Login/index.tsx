import React from "react";
import { Text, TouchableOpacity, TextInput, Image, View } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { onChange } from "react-native-reanimated";
import api from "../../services/api";

import {
  AppScreen,
  TabBarScreen,
  TabBarText,
  MainScreen,
  Title,
  InputBoxText,
  RegisterButton,
} from "../RegisterPages/style";

import Button from "../../components/Button";
import { useNavigation } from "@react-navigation/native";

interface FormData {
  username: string;
  password: string;
}

export default function Login() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    api
      .post("/login", data)
      .then((response) => {
        alert("Login feito com sucesso!");
        navigation.navigate("HomeServices", response.data);
      })
      .catch(function (error) {
        console.log(error.toJSON());
        alert("Não foi possível logar");
      });
  };

  const navigation = useNavigation();

  return (
    <AppScreen>
      <TabBarScreen>
        <Image source={require("../../../assets/icon-civitas.png")} />
        <TouchableOpacity onPress={() => navigation.navigate("HomeServices")}>
          <TabBarText>Navegar sem conta</TabBarText>
        </TouchableOpacity>
      </TabBarScreen>

      <MainScreen>
        <Title>Login</Title>

        <View style={InputBoxText.inputBox}>
          <Controller
            control={control}
            render={({ field: { onBlur, onChange, value } }) => (
              <TextInput
                autoCompleteType="name"
                textContentType="name"
                placeholder="Nome de usuário"
                maxLength={15}
                onBlur={onBlur}
                onChangeText={(value: any) => onChange(value)}
                value={value}
                style={InputBoxText.text}
              />
            )}
            rules={{
              required: "O nome de usuário é obrigatório",
            }}
            name="username"
            defaultValue=""
          />
          {errors.username && (
            <Text style={{ color: "red" }}>{errors.username.message}</Text>
          )}
        </View>

        <View style={InputBoxText.inputBox}>
          <Controller
            control={control}
            render={({ field: { onBlur, onChange, value } }) => (
              <TextInput
                placeholder="Digite sua senha"
                maxLength={20}
                secureTextEntry
                onBlur={onBlur}
                onChangeText={(value: any) => onChange(value)}
                value={value}
                style={InputBoxText.text}
              />
            )}
            rules={{
              required: "A senha é obrigatória",
            }}
            name="password"
            defaultValue=""
          />
          {errors.password && (
            <Text style={{ color: "red" }}>{errors.password.message}</Text>
          )}
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate("RecoverPassword")}
        >
          <Text style={{ marginTop: 10, fontSize: 16 }}>
            Esqueci minha senha
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={InputBoxText.button}
          onPress={handleSubmit(onSubmit)}
        >
          <Button value="Entrar" />
        </TouchableOpacity>

        <RegisterButton>
          <TouchableOpacity
            onPress={() => navigation.navigate("Register")}
            style={{ flexDirection: "row" }}
          >
            <Text style={{ marginTop: "20%", fontSize: 16 }}>
              Ainda não tem conta?
            </Text>
            <Text
              style={{ fontWeight: "bold", marginTop: "20%", fontSize: 16 }}
            >
              {" "}
              Faça agora!
            </Text>
          </TouchableOpacity>
        </RegisterButton>
      </MainScreen>
    </AppScreen>
  );
}
