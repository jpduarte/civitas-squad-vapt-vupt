import React from "react";
import Header from "../../components/Header";
import NavigationBar from "../../components/NavigationBar/Cart";
import {
  HomeView,
  FeedView,
  SalesTitle,
  CartText,
  CartTextTitle,
  CartTotalView,
  CartTotalText,
} from "../style";
import { NavigationTextSelected, NavigationViewCart } from "../style";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Button from "../../components/Button";
import CartItem from "../../components/CartItem";

import "intl";
import "intl/locale-data/jsonp/pt-BR";
import { Alert, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function CartScreen() {
  const navigation = useNavigation();

  return (
    <HomeView>
      <Header
        actionLogout={() => navigation.navigate("Login")}
        actionProfile={() => navigation.navigate("Profile")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <FeedView>
          <NavigationViewCart>
            <SalesTitle>Meu Carrinho</SalesTitle>
            <TouchableOpacity onPress={() => {}}>
              <NavigationTextSelected>Limpar</NavigationTextSelected>
            </TouchableOpacity>
          </NavigationViewCart>
          <CartItem
            quantity={3}
            name={"Tomate 300g"}
            total={Intl.NumberFormat("pt-BR", {
              style: "currency",
              currency: "BRL",
            }).format(2.56)}
            picture={
              <Image
                source={require("../../../assets/cart-item-example.png")}
              />
            }
          />
          <CartItem
            quantity={3}
            name={"Alho un."}
            total={Intl.NumberFormat("pt-BR", {
              style: "currency",
              currency: "BRL",
            }).format(2.96)}
            picture={
              <Image
                source={require("../../../assets/cart-item-example-2.png")}
              />
            }
          />
          <TouchableOpacity>
            <CartText>Adicionar mais itens</CartText>
          </TouchableOpacity>
          <CartTextTitle>Resumo de valores</CartTextTitle>
          <CartTotalView>
            <CartTotalText>Sub-Total</CartTotalText>
            <CartTotalText>
              {Intl.NumberFormat("pt-BR", {
                style: "currency",
                currency: "BRL",
              }).format(5.52)}
            </CartTotalText>
          </CartTotalView>
          <CartTotalView>
            <CartTotalText>Taxa de entrega</CartTotalText>
            <CartTotalText>GRÁTIS</CartTotalText>
          </CartTotalView>
          <CartTotalView>
            <CartText style={{ textTransform: "uppercase" }}>total</CartText>
            <CartText>
              {Intl.NumberFormat("pt-BR", {
                style: "currency",
                currency: "BRL",
              }).format(5.52)}
            </CartText>
          </CartTotalView>
          <TouchableOpacity
            onPress={() => {
              Alert.alert(
                "Compra finalizada com sucesso",
                "Continuar para home",
                [
                  {
                    text: "Continuar",
                    onPress: () => navigation.navigate("HomeServices"),
                  },
                ]
              );
            }}
          >
            <Button value={"Finalizar compra"} />
          </TouchableOpacity>
        </FeedView>
      </ScrollView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionFavorite={() => {
          navigation.navigate("UserFavoriteServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
      />
    </HomeView>
  );
}
