import { StyleSheet } from "react-native";

import styled from "styled-components/native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export const HomeView = styled.View`
  width: ${wp("100%")};
  height: ${hp("100%")};
  display: flex;
  background-color: #ffffff;
`;

export const FeedView = styled.View`
  flex: 1;
  padding: 5%;
`;

export const NavigationView = styled.View`
  display: flex;
  flex-direction: row;
`;

export const NavigationViewCart = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const CartText = styled.Text`
  font-size: 18px;
  color: #ff5894;
  align-self: center;
  margin-bottom: 5%;
`;

export const CartTextTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #ff5894;
  margin-bottom: 2%;
`;

export const CartTotalView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const CartTotalText = styled.Text`
  font-size: 18px;
  color: #ff9bbf;
  margin-bottom: 2%;
`;

export const NavigationText = styled.Text`
  font-size: 20px;
  padding: 0 10% 5% 0;
`;

export const NavigationTextSelected = styled.Text`
  font-size: 20px;
  color: #ff5894;
  padding: 0 10% 5% 0;
`;

export const ItemsView = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const InputView = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: 5%;
  align-items: center;
`;

export const SalesContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: baseline;
`;

export const SalesTitle = styled.Text`
  font-size: 24px;
  color: #ff5894;
  margin-bottom: 3%;
  font-weight: bold;
  letter-spacing: 1px;
`;

export const AddNewSale = styled.View`
  border: 1px solid #ff5894;
`;

export const AddNewSaleText = styled.Text`
  color: #ff5894;
  font-size: 16px;
  padding: 2%;
`;

export const FeedImage = StyleSheet.create({
  image: {
    marginBottom: "30%",
    borderRadius: 5,
  },
});

export const InputStyle = StyleSheet.create({
  inputBox: {
    borderWidth: 1,
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingVertical: 5,
    width: "90%",
    marginRight: "2%",
  },
});
