import React from "react";
import Header from "../../../components/Header";
import { TouchableOpacity, View } from "react-native";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { useForm, Controller } from "react-hook-form";
import { TextInputMask } from "react-native-masked-text";

import "intl";
import "intl/locale-data/jsonp/pt-BR";

import Button from "../../../components/Button";
import ButtonAddCart from "../../../components/ButtonAddCart";
import NavigationBar from "../../../components/NavigationBar/Default";

import {
  HomeView,
  FeedView,
  NavigationView,
  NavigationTextSelected,
  DescriptionView,
  CommentView,
  ImageStyle,
  ImageStyle2,
  ImageStyle3,
  ImageStyle4,
  ImageView,
  ProductTitle,
  ProductSubtitle,
  CommentTitleView,
  CommentSubtitleView,
  CommentSubtitle,
  CommentedView,
  ButtonCartView,
  InputBoxText,
  InputBoxDate,
  TitleView,
  CommentarieView,
} from "../../ProductsServices/style";
import { useNavigation } from "@react-navigation/native";

export default function ServicePage() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  const navigation = useNavigation();

  interface FormData {
    title: string;
    description: string;
    date: string;
  }

  return (
    <HomeView>
      <Header
        actionLogout={() => navigation.navigate("Login")}
        actionProfile={() => navigation.navigate("Profile")}
      />
      <ScrollView>
        <FeedView>
          <NavigationView>
            <NavigationTextSelected>Serviços</NavigationTextSelected>
            <ImageStyle
              source={require("../../../../assets/pointer.png")}
              resizeMode="contain"
            />
            <NavigationTextSelected>Barbeiro</NavigationTextSelected>
          </NavigationView>

          <ImageView>
            <ImageStyle2
              source={require("../../../../assets/image-service.png")}
              resizeMode="contain"
            />
          </ImageView>

          <DescriptionView>
            <View>
              <View>
                <ProductTitle>Barbeiro</ProductTitle>
              </View>
              <View>
                <ProductSubtitle>Corte masculino de cabelo</ProductSubtitle>
              </View>
            </View>
            <View>
              <TouchableOpacity>
                <ImageStyle3
                  source={require("../../../../assets/Like.png")}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </DescriptionView>

          <ButtonCartView>
            <View>
              <NavigationTextSelected>
                {Intl.NumberFormat("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                }).format(35.0)}
              </NavigationTextSelected>
            </View>
            <View>
              <TouchableOpacity>
                <ButtonAddCart value="Adicionar ao carrinho" />
              </TouchableOpacity>
            </View>
          </ButtonCartView>

          <CommentView>
            <ImageStyle4
              source={require("../../../../assets/image-user3.png")}
              resizeMode="contain"
            />
            <TitleView>
              <CommentTitleView>
                <ProductSubtitle>Precinho Bacana</ProductSubtitle>
                <ProductSubtitle>04/11/2021</ProductSubtitle>
              </CommentTitleView>
              <CommentSubtitleView>
                <CommentSubtitle>Ótimo preço e produto!</CommentSubtitle>
              </CommentSubtitleView>
            </TitleView>
          </CommentView>

          <CommentView>
            <ImageStyle4
              source={require("../../../../assets/image-user2.png")}
            />

            <TitleView>
              <CommentTitleView>
                <ProductSubtitle>Necessário</ProductSubtitle>
                <ProductSubtitle>04/11/2021</ProductSubtitle>
              </CommentTitleView>
              <CommentSubtitleView>
                <CommentSubtitle>E o preço é muito bom</CommentSubtitle>
              </CommentSubtitleView>
            </TitleView>
          </CommentView>

          <CommentedView>
            <CommentarieView>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Titulo"
                    maxLength={15}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                rules={{
                  required: "O titulo é obrigatório",
                }}
                name="title"
                defaultValue=""
              />
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInputMask
                    placeholder="Data"
                    type={"datetime"}
                    options={{
                      format: "DD/MM/YYYY",
                    }}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxDate.text}
                  />
                )}
                rules={{
                  required: "O titulo é obrigatório",
                }}
                name="date"
                defaultValue=""
              />
            </CommentarieView>

            <CommentSubtitleView>
              <Controller
                control={control}
                render={({ field: { onBlur, onChange, value } }) => (
                  <TextInput
                    placeholder="Descrição"
                    maxLength={8}
                    onBlur={onBlur}
                    onChangeText={(value: any) => onChange(value)}
                    value={value}
                    style={InputBoxText.text}
                  />
                )}
                name="description"
                defaultValue=""
              />
            </CommentSubtitleView>
          </CommentedView>

          <TouchableOpacity>
            <Button value="Publicar" />
          </TouchableOpacity>
        </FeedView>
      </ScrollView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionFavorite={() => {
          navigation.navigate("UserFavoriteServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
        actionCart={() => {
          navigation.navigate("Cart");
        }}
      />
    </HomeView>
  );
}
