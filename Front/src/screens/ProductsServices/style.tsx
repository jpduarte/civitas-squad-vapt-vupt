import { StyleSheet } from "react-native";

import styled from "styled-components/native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export const HomeView = styled.View`
  width: ${wp("100%")};
  height: ${hp("100%")};
  display: flex;
  background-color: #ffffff;
`;

export const FeedView = styled.View`
  flex: 1;
  padding: 5%;
`;

export const NavigationView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const NavigationTextSelected = styled.Text`
  font-size: 20px;
  color: #ff5894;
`;

export const DescriptionView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 25px;
  align-items: center;
`;

export const ButtonCartView = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 25px;
  align-items: center;
  justify-content: space-between;
`;

export const ImageStyle = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 10%;
  height: 9px;
`;
export const ImageStyle2 = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 150px;
  height: 150px;
  border-radius: 8px;
`;

export const ImageStyle3 = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 30px;
  height: 30px;
`;

export const ImageStyle4 = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 60px;
  height: 85px;
`;

export const ImageStyle5 = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 23px;
  height: 22px;
`;

export const ImageStyle6 = styled.Image`
  opacity: 1;
  z-index: 0;
  width: 20px;
  height: 20px;
`;

export const ImageView = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 30px;
`;

export const ImageView2 = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 8px;
  align-items: center;
`;

export const ButtonView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 8px;
  align-items: center;
  width: 130px;
  height: 23px;
`;

export const ProductTitle = styled.Text`
  font-size: 24px;
  color: #ff5894;
  margin-bottom: 3%;
  font-weight: bold;
  letter-spacing: 1px;
`;

export const ProductSubtitle = styled.Text`
  font-size: 14px;
  color: #ff5894;
  margin-bottom: 3%;
  letter-spacing: 1px;
`;

export const CommentView = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 30px;
  width: 100%;
  border-radius: 5px;
  border: 2px #ff5894;
`;

export const CommentTitleView = styled.View`
  border-radius: 5px;
  border: 2px #ff5894;
  padding-left: 5px;
  flex-direction: row;
  justify-content: space-between;
  
`;

export const CommentarieView = styled.View`
  border-radius: 5px;
  border: 2px #ff5894;
  padding-left: 5px;
  flex-direction: row;
  justify-content:space-around;
  width: 100%;
`;

export const CommentSubtitleView = styled.View`
  border-radius: 5px;
  padding-left: 5px;
  width: 100%;
  margin-top: 5px;
  flex-grow: 1;
`;

export const CommentSubtitle = styled.Text`
  font-size: 12px;
  color: #ff5894;
  margin-bottom: 3%;
  letter-spacing: 1px;
`;

export const CommentedView = styled.View`
  display: flex;
  margin-top: 30px;
  margin-bottom: 30px;
  height: 150px;
  border-radius: 5px;
  border: 2px #ff5894;
  align-items: center;
`;

export const CommentedTitleView = styled.View`
  border-radius: 5px;
  border: 2px #ff5894;
  padding-left: 5px;
  width: 100%;
`;

export const TitleView = styled.View`
  flex-grow: 1;
`;

export const QuantityView = styled.View`
  align-items: flex-end;
`;

export const InputBoxText = StyleSheet.create({
  text: {
    color: "#FF5894",
    fontSize: 13,
    height: "100%",
  },
});

export const InputBoxDate = StyleSheet.create({
  text: {
    color: "#FF5894",
    fontSize: 13,
  },
});
