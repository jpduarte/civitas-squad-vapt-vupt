import React, { useState } from "react";
import Header from "../../../components/Header";
import { KeyboardAvoidingView, TouchableOpacity, View } from "react-native";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { useForm, Controller } from "react-hook-form";
import { TextInputMask } from "react-native-masked-text";
import NavigationBar from "../../../components/NavigationBar/Default";

import "intl";
import "intl/locale-data/jsonp/pt-BR";

import Button from "../../../components/Button";
import InputSpinner from "react-native-input-spinner";

import {
  HomeView,
  FeedView,
  NavigationView,
  NavigationTextSelected,
  DescriptionView,
  CommentView,
  ImageStyle,
  ImageStyle2,
  ImageStyle3,
  ImageStyle4,
  ImageStyle5,
  ImageStyle6,
  ImageView,
  ImageView2,
  ProductTitle,
  ProductSubtitle,
  CommentTitleView,
  CommentSubtitleView,
  CommentSubtitle,
  CommentedView,
  CommentedTitleView,
  QuantityView,
  InputBoxText,
  InputBoxDate,
  TitleView,
  CommentarieView,
} from "../../ProductsServices/style";
import { useNavigation } from "@react-navigation/native";

export default function ProductPage() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  const [price, setPrice] = useState(2.5);

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  const navigation = useNavigation();

  interface FormData {
    title: string;
    description: string;
    date: string;
  }
  return (
    <HomeView>
      <Header
        actionLogout={() => navigation.navigate("Login")}
        actionProfile={() => navigation.navigate("Profile")}
      />
      <ScrollView>
        <FeedView>
          <NavigationView>
            <NavigationTextSelected>Produtos</NavigationTextSelected>
            <ImageStyle
              source={require("../../../../assets/pointer.png")}
              resizeMode="contain"
            />
            <NavigationTextSelected>Hamburguer</NavigationTextSelected>
          </NavigationView>

          <ImageView>
            <ImageStyle2
              source={require("../../../../assets/default-item-2.png")}
              resizeMode="contain"
            />
          </ImageView>

          <DescriptionView>
            <View>
              <View>
                <ProductTitle>Hamburger</ProductTitle>
              </View>
              <View>
                <ProductSubtitle>Vendida a unidade</ProductSubtitle>
              </View>
            </View>
            <View>
              <TouchableOpacity>
                <ImageStyle3
                  source={require("../../../../assets/Like.png")}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          </DescriptionView>

          <DescriptionView>
            <View>
              <NavigationTextSelected>
                {Intl.NumberFormat("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                }).format(price)}
              </NavigationTextSelected>
            </View>
            <QuantityView>
              <InputSpinner
                buttonStyle={{ height: 50 }}
                style={{
                  shadowColor: "transparent",
                  minHeight: 10,
                  height: 50,
                }}
                min={1}
                step={1}
                color={"#FF5894"}
                value={1}
                skin="modern"
                onChange={(num: any) => {
                  setPrice(12.5 * num);
                }}
              />
            </QuantityView>
          </DescriptionView>

          <CommentView>
            <ImageStyle4
              source={require("../../../../assets/image-user1.png")}
              resizeMode="contain"
            />
            <TitleView>
              <CommentTitleView>
                <ProductSubtitle>Precinho Bacana</ProductSubtitle>
                <ProductSubtitle>04/11/2021</ProductSubtitle>
              </CommentTitleView>
              <CommentSubtitleView>
                <CommentSubtitle>Ótimo preço e produto!</CommentSubtitle>
                <TouchableOpacity style={{ alignItems: "flex-end" }}>
                  <ImageStyle6
                    source={require("../../../../assets/icon-trash.png")}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </CommentSubtitleView>
            </TitleView>
          </CommentView>

          <CommentView>
            <ImageStyle4
              source={require("../../../../assets/image-user2.png")}
            />

            <TitleView>
              <CommentTitleView>
                <ProductSubtitle>Necessário</ProductSubtitle>
                <ProductSubtitle>04/11/2021</ProductSubtitle>
              </CommentTitleView>
              <CommentSubtitleView>
                <CommentSubtitle>E o preço é muito bom</CommentSubtitle>
              </CommentSubtitleView>
            </TitleView>
          </CommentView>

          <KeyboardAvoidingView
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
            }}
          >
            <CommentedView>
              <CommentarieView>
                <Controller
                  control={control}
                  render={({ field: { onBlur, onChange, value } }) => (
                    <TextInput
                      placeholder="Titulo"
                      maxLength={15}
                      onBlur={onBlur}
                      onChangeText={(value: any) => onChange(value)}
                      value={value}
                      style={InputBoxText.text}
                    />
                  )}
                  rules={{
                    required: "O titulo é obrigatório",
                  }}
                  name="title"
                  defaultValue=""
                />
                <Controller
                  control={control}
                  render={({ field: { onBlur, onChange, value } }) => (
                    <TextInputMask
                      placeholder="Data"
                      type={"datetime"}
                      options={{
                        format: "DD/MM/YYYY",
                      }}
                      onBlur={onBlur}
                      onChangeText={(value: any) => onChange(value)}
                      value={value}
                      style={InputBoxDate.text}
                    />
                  )}
                  rules={{
                    required: "O titulo é obrigatório",
                  }}
                  name="date"
                  defaultValue=""
                />
              </CommentarieView>
              <CommentSubtitleView>
                <Controller
                  control={control}
                  render={({ field: { onBlur, onChange, value } }) => (
                    <TextInput
                      placeholder="Descrição"
                      maxLength={200}
                      onBlur={onBlur}
                      onChangeText={(value: any) => onChange(value)}
                      value={value}
                      style={InputBoxText.text}
                      multiline={true}
                      numberOfLines={4}
                    />
                  )}
                  name="description"
                  defaultValue=""
                />
              </CommentSubtitleView>
            </CommentedView>
          </KeyboardAvoidingView>

          <TouchableOpacity>
            <Button value="Publicar" />
          </TouchableOpacity>
        </FeedView>
      </ScrollView>
      <NavigationBar
        actionHome={() => {
          navigation.navigate("HomeServices");
        }}
        actionFavorite={() => {
          navigation.navigate("UserFavoriteServices");
        }}
        actionSales={() => {
          navigation.navigate("UserServices");
        }}
        actionCart={() => {
          navigation.navigate("Cart");
        }}
      />
    </HomeView>
  );
}
