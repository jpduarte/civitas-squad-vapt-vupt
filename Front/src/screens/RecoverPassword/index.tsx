import React from "react";
import { Text, TouchableOpacity, TextInput, Image, View } from "react-native";
import { useForm, Controller } from "react-hook-form";

import {
  AppScreen,
  TabBarScreen,
  TabBarText,
  MainScreen,
  Title,
  InputBoxText,
} from "../RegisterPages/style";

import Button from "../../components/Button";
import { useNavigation } from "@react-navigation/native";

interface FormData {
  email: string;
  password: string;
}

export default function RecoverPassword() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  const onSubmit = (data: FormData) => {
    console.log(data);
  };

  const navigation = useNavigation();

  return (
    <AppScreen>
      <TabBarScreen>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <TabBarText>Voltar</TabBarText>
        </TouchableOpacity>
        <Image source={require("../../../assets/icon-civitas.png")} />
      </TabBarScreen>

      <MainScreen>
        <Title>Recuperar senha</Title>

        <View style={InputBoxText.inputBox}>
          <Controller
            control={control}
            render={({ field: { onBlur, onChange, value } }) => (
              <TextInput
                placeholder="Digite seu e-mail"
                maxLength={20}
                onBlur={onBlur}
                onChangeText={(value: any) => onChange(value)}
                value={value}
                style={InputBoxText.text}
              />
            )}
            rules={{
              required: "Campo obrigatório",
              pattern: {
                value: /^\S+@\S+$/i,
                message: "Formato inválido",
              },
            }}
            name="email"
            defaultValue=""
          />
          {errors.email && (
            <Text style={{ color: "red" }}>{errors.email.message}</Text>
          )}
        </View>

        <TouchableOpacity
          style={InputBoxText.button}
          onPress={
            (handleSubmit(onSubmit),
            () => {
              navigation.navigate("Login");
            })
          }
        >
          <Button value="Enviar" />
        </TouchableOpacity>
      </MainScreen>
    </AppScreen>
  );
}
