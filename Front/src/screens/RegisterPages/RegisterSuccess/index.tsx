import React from "react";
import { TouchableOpacity, Image } from "react-native";

import {
  AppScreen,
  TabBarScreen,
  TabBarText,
  MainScreen,
  TitleSuccess,
  InputBoxText,
  Subtitle,
} from "../style";

import Button from "../../../components/Button";

import { useNavigation } from "@react-navigation/native";

export default function RegisterSuccess() {
  const navigation = useNavigation();
  return (
    <AppScreen>
      <TabBarScreen>
        <Image source={require("../../../../assets/icon-civitas.png")} />
      </TabBarScreen>

      <MainScreen>
        <TitleSuccess>Cadastro realizado com sucesso!</TitleSuccess>
        <Subtitle>
          Confira sua caixa de entrada para verificar seu e-mail
        </Subtitle>

        <TouchableOpacity
          onPress={() => navigation.navigate("Login")}
          style={InputBoxText.button}
        >
          <Button value="Continuar" />
        </TouchableOpacity>
      </MainScreen>
    </AppScreen>
  );
}
