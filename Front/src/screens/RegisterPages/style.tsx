import styled from "styled-components/native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import { StyleSheet } from "react-native";

export const AppScreen = styled.View`
  padding-top: 8%;
  background-color: #d9fde3;
  width: ${wp("100%")};
  height: ${hp("100%")};
  flex: 1;
`;

export const TabBarScreen = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-bottom: 5%;
  padding: 0 5% 5% 5%;
  justify-content: space-between;
`;

export const TabBarText = styled.Text`
  color: #ff5894;
  font-size: 18px;
`;

export const MainScreen = styled.View`
  background-color: #ffffff;
  height: ${hp("100%")};
  border-top-right-radius: 50;
  border-top-left-radius: 50;
  display: flex;
  padding: 5% 8%;
  flex: 1;
`;

export const Title = styled.Text`
  color: #ff5894;
  font-size: 32px;
  font-weight: bold;
  letter-spacing: 1px;
  margin-bottom: 4%;
`;

export const TitleSuccess = styled.Text`
  color: #ff5894;
  font-size: 34px;
  font-weight: bold;
  padding: 0 10%;
  letter-spacing: 1px;
  margin-bottom: 4%;
  text-align: center;
`;

export const Subtitle = styled.Text`
  color: #000;
  font-size: 24px;
  letter-spacing: 1px;
  margin-top: 40%;
  margin-bottom: 20%;
  text-align: center;
`;

export const RegisterButton = styled.View`
  text-align: center;
  margin-top: 10%;
  flex-direction: row;
  justify-content: center;

`;

export const InputBoxText = StyleSheet.create({
  inputBox: {
    borderColor: "#000",
    borderWidth: 1,
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 10,
  },

  text: {
    color: "#000",
    fontSize: 18,
    paddingLeft: 10,
  },

  button: {
    marginTop: 15,
  },
});
