import React from "react";
import {
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  View,
  ScrollView,
} from "react-native";
import { useForm, Controller } from "react-hook-form";
import { onChange } from "react-native-reanimated";

import {
  AppScreen,
  TabBarScreen,
  TabBarText,
  MainScreen,
  Title,
  InputBoxText,
} from "../style";

import { TextInputMask } from "react-native-masked-text";

import Button from "../../../components/Button";
import { useNavigation } from "@react-navigation/native";
import api from "../../../services/api";

export default function Register() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onTouched" });

  interface FormData {
    name: string;
    username: string;
    email: string;
    password: string;
    birthDate: string;
    address: string;
  }
  const onSubmit = async (data: FormData) => {
    console.log(data.password)
    await api.post("/register", {
      name: data.name,
      email: data.email,  
      username: data.username,
      address: data.address,
      password: data.password
    }).then(
      (response) => {
        console.log("Cadastro feito com sucesso!");
        navigation.navigate("RegisterSuccess");
      },
      (errors) => console.log(errors.toJSON)
    );
  };
  const onError = (errors: Object) => {
    console.log(errors);
  };

  const navigation = useNavigation();

  return (
    <AppScreen>
      <TabBarScreen>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <TabBarText>Voltar</TabBarText>
        </TouchableOpacity>
        <Image source={require("../../../../assets/icon-civitas.png")} />
      </TabBarScreen>

      <MainScreen>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Title>Cadastrar</Title>

          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Nome"
                  autoCompleteType="name"
                  textContentType="name"
                  maxLength={25}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "O nome é obrigatório",
              }}
              name="name"
              defaultValue=""
            />
            {errors.name && (
              <Text style={{ color: "red" }}>{errors.name.message}</Text>
            )}
          </View>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  autoCompleteType="name"
                  textContentType="name"
                  placeholder="Nome de usuário"
                  maxLength={15}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "O nome de usuário é obrigatório",
              }}
              name="username"
              defaultValue=""
            />
            {errors.username && (
              <Text style={{ color: "red" }}>{errors.username.message}</Text>
            )}
          </View>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="E-mail"
                  maxLength={20}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "O e-mail é obrigatório",
                pattern: {
                  value: /^\S+@\S+$/i,
                  message: "Formato inválido",
                },
              }}
              name="email"
              defaultValue=""
            />
            {errors.email && (
              <Text style={{ color: "red" }}>{errors.email.message}</Text>
            )}
          </View>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Senha"
                  maxLength={20}
                  secureTextEntry
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "A senha é obrigatória",
              }}
              name="password"
              defaultValue=""
            />
            {errors.password && (
              <Text style={{ color: "red" }}>{errors.password.message}</Text>
            )}
          </View>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInputMask
                  placeholder="Data de nascimento"
                  type={"datetime"}
                  options={{
                    format: "DD/MM/YYYY",
                  }}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              name="birthDate"
              defaultValue=""
            />
          </View>
          <View style={InputBoxText.inputBox}>
            <Controller
              control={control}
              render={({ field: { onBlur, onChange, value } }) => (
                <TextInput
                  placeholder="Bairro"
                  maxLength={25}
                  onBlur={onBlur}
                  onChangeText={(value: any) => onChange(value)}
                  value={value}
                  style={InputBoxText.text}
                />
              )}
              rules={{
                required: "O bairro é obrigatório",
              }}
              name="address"
              defaultValue=""
            />
            {errors.address && (
              <Text style={{ color: "red" }}>{errors.address.message}</Text>
            )}
          </View>

          <TouchableOpacity
            style={InputBoxText.button}
            onPress={handleSubmit(onSubmit,onError)}
          >
            <Button value="Finalizar" />
          </TouchableOpacity>
        </ScrollView>
      </MainScreen>
    </AppScreen>
  );
}
