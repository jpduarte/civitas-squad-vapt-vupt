import React from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";

import {
  CartItemView,
  CartItemPriceView,
  QtNameText,
  TotalText,
  OptionsView,
} from "./style";

export type Props = {
  quantity: number;
  name: string;
  total: string;
  picture: any;
  actionRemove?: void;
  actionAdd?: void;
  actionDelete?: void;
};

export const CartItem: React.FC<Props> = ({
  quantity = 0,
  name,
  total = 0,
  picture,
  actionRemove,
  actionAdd,
  actionDelete,
}) => {
  return (
    <CartItemView>
      <CartItemPriceView>
        <QtNameText>
          {quantity}x {name}
        </QtNameText>
        <TotalText>{total}</TotalText>
        <OptionsView>
          <TouchableOpacity
            style={{ marginRight: 10 }}
            onPress={() => {
              actionRemove;
            }}
          >
            <Image source={require("../../../assets/minus-icon.png")} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ marginRight: 50 }}
            onPress={() => {
              actionAdd;
            }}
          >
            <Image source={require("../../../assets/add-icon.png")} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              actionDelete;
            }}
          >
            <Image source={require("../../../assets/trash-cart-icon.png")} />
          </TouchableOpacity>
        </OptionsView>
      </CartItemPriceView>
      {picture}
    </CartItemView>
  );
};

export default CartItem;
