import styled from "styled-components/native";

export const CartItemView = styled.View`
  background-color: #ffc3d9;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 3% 5%;
  margin-bottom: 5%;
  border-radius: 10;
  align-items: center;
`;

export const CartItemPriceView = styled.View`
  display: flex;
`;

export const QtNameText = styled.Text`
  font-size: 20px;
  color: #ff5894;
`;

export const TotalText = styled.Text`
  font-size: 18px;
  letter-spacing: 1px;
  margin-bottom: 8%;
  color: #ffffff;
`;
export const OptionsView = styled.View`
  display: flex;
  flex-direction: row;
`;
