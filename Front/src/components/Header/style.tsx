import React from "react";
import styled from "styled-components/native";

export const HeaderApp = styled.View`
  background-color: #d9fde3;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10% 10% 5% 5%;
  justify-content: space-between;
  padding-top: 8%;
  width: 100%;
`;

export const HeaderLeft = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const HeaderText = styled.Text`
  margin-left: 20;
  font-size: 18;
  color: #ff5894;
`;

export const ProfilePicture = styled.View`
  background-color: #ff5894;
  border-radius: 50px;
  width: 50px;
  height: 50px;
`;
