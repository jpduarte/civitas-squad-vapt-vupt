import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Image, Text } from "react-native";

import { HeaderApp, HeaderLeft, HeaderText, ProfilePicture } from "./style";

export type Props = {
  actionProfile?: any;
  actionLogout?: any;
};

export const Header: React.FC<Props> = ({ actionProfile, actionLogout }) => {
  return (
    <HeaderApp>
      <HeaderLeft>
        <TouchableOpacity onPress={actionProfile}>
          <ProfilePicture>
            <Text>Picture</Text>
          </ProfilePicture>
        </TouchableOpacity>
        <HeaderText>Olá, Lucicreia!</HeaderText>
      </HeaderLeft>
      <TouchableOpacity onPress={actionLogout}>
        <Image source={require("../../../assets/logout.png")} />
      </TouchableOpacity>
    </HeaderApp>
  );
};

export default Header;
