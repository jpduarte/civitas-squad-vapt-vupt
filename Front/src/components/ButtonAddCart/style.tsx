import styled from "styled-components/native";

export const BackgroundButtonCart = styled.View`
  background-color: #ff5894;
  padding: 10px;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
`;

export const TextButtonCart = styled.Text`
  color: #ffffff;
  font-weight: bold;
  font-size: 13px;
  letter-spacing: 2px;
`;
