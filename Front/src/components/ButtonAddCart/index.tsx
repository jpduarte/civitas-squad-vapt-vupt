import React from "react";

import { TouchableOpacity, Text, Touchable } from "react-native";
import { BackgroundButtonCart, TextButtonCart } from "./style";

type ButtonData = {
  value: string;
};

const ButtonAddCart: React.FC<ButtonData> = ({ value }) => {
    return (
      <BackgroundButtonCart>
        <TextButtonCart>{value}</TextButtonCart>
      </BackgroundButtonCart>
    );
  };
  
  export default ButtonAddCart;