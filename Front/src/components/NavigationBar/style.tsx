import { StyleSheet } from "react-native";
import styled from "styled-components/native";

export const NavView = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-end;
  background-color: #d9fde3;
  padding: 5% 10% 5% 10%;
`;

export const ModalCenter = styled.View`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: flex-end;
  margin-top: 52%;
  margin-right: 10%;
`;

export const ModalView = styled.View`
  background-color: #ffffff;
  border-radius: 10;
  padding: 5% 10% 10% 10%;
  display: flex;
  justify-content: flex-start;
`;

export const ModalText = styled.Text`
  font-size: 22px;
  color: #000;
  letter-spacing: 1px;
  margin-bottom: 5%;
`;

export const ModalTextButton = styled.Text`
  background-color: #ff5894;
  border-radius: 5;
  text-align: center;
  margin: 2% 0 3% 0;
  padding: 5%;
  color: #ffffff;
  font-size: 16px;
`;
