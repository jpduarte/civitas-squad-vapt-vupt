import React from "react";

import { Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { NavView } from "../style";

export type Props = {
  actionHome?: any;
  actionFavorite?: any;
  actionSales?: any;
  actionCart?: any;
};

export const NavigationBar: React.FC<Props> = ({
  actionHome,
  actionFavorite,
  actionSales,
  actionCart,
}) => {
  return (
    <NavView>
      <TouchableOpacity onPress={actionHome}>
        <Image source={require("../../../../assets/home-icon.png")} />
      </TouchableOpacity>
      <TouchableOpacity onPress={actionFavorite}>
        <Image source={require("../../../../assets/favorite-icon.png")} />
      </TouchableOpacity>
      <TouchableOpacity onPress={actionSales}>
        <Image source={require("../../../../assets/sales-icon.png")} />
      </TouchableOpacity>
      <TouchableOpacity onPress={actionCart}>
        <Image source={require("../../../../assets/cart-icon.png")} />
      </TouchableOpacity>
    </NavView>
  );
};

export default NavigationBar;
