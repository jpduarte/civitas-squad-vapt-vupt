import React from "react";

import { TouchableOpacity, Text, Touchable } from "react-native";
import { BackgroundButton, TextButton } from "./style";

type ButtonData = {
  value: string;
};

const Button: React.FC<ButtonData> = ({ value }) => {
  return (
    <BackgroundButton>
      <TextButton>{value}</TextButton>
    </BackgroundButton>
  );
};

export default Button;
