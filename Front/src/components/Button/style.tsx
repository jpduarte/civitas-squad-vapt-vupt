import styled from "styled-components/native";

export const BackgroundButton = styled.View`
  background-color: #ff5894;
  padding: 20px;
  display: flex;
  border-radius: 10;
  justify-content: center;
  align-items: center;
`;

export const TextButton = styled.Text`
  color: #ffffff;
  font-weight: bold;
  font-size: 28px;
  letter-spacing: 2px;
`;
