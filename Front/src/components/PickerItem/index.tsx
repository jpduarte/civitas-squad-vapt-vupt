import React, { useState } from "react";
import { View, Picker, StyleSheet } from "react-native";

export default function PickerItem({
  optionLabelOne,
  optionValueOne,
  optionLabelTwo,
  optionValueTwo,
}) {
  const [selectedValue, setSelectedValue] = useState({ optionValueOne });
  return (
    <View style={styles.container}>
      <Picker
        selectedValue={selectedValue}
        style={{ height: 50, width: 150 }}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
      >
        <Picker.Item label={optionLabelOne} value={optionValueOne} />
        <Picker.Item label={optionLabelTwo} value={optionValueTwo} />
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
