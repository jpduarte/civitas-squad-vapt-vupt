<div align="center" id="top">

# Civitas #

</div>

&#xa0;

<div align="center">

<p>
  <a href="#dart-about"> About </a> &#xa0; | &#xa0;
  <a href="art-prototyping"> Prototyping </a> &#xa0; | &#xa0;  
  <a href="#game_die-database-modeling"> Database modeling </a> &#xa0; | &#xa0;  
  <a href="#computer-technologies-used"> Technologies used </a> &#xa0; | &#xa0; 
  <a href="#white_check_mark-requirements"> Requirements </a> &#xa0; | &#xa0; 
  <a href="#checkered_flag-starting"> Starting </a> &#xa0; | &#xa0; 
  <a href="#bulb-roadmap"> Roadmap </a> &#xa0; | &#xa0;
  <a href="#unlock-license"> License </a> &#xa0; | &#xa0;
  <a href="#busts_in_silhouette-team "> Team </a>
</p>

</div>

<br>

## :dart: About ##

Civitas is a marketplace of products and services created by the local community and for the community. It increases the dissemination of local commerce and service with a concern for accessibility. Civitas goals are to save the user's time and boost the region's economy. Our target audience is merchants, local service providers and the local population.

This project was proposed in the <a href='https://ejcm.com.br/'> EJCM </a> selection process.

## :art: Prototyping ##

Prototyping done in <a href="https://www.figma.com/file/wfX651PnXzqOaPOFaabhTv/marketplace-ejcm?node-id=1%3A4">Figma<a/>.

![Protottyping](https://i.imgur.com/Lo6tpDr.png)
    
## :game_die: Database modeling ##

Database modeling done in <a href="https://github.com/chcandido/brModelo">brModelo<a/>.
        
<div align="center">

![Database Modeling](https://i.imgur.com/A5OlcH1.png)

</div>
    
## :computer: Technologies used ##

Front-end | Back-end | 
--- | --- 
[Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript) | [NodeJS](https://nodejs.org/pt-br/) |
[CSS](https://developer.mozilla.org/pt-BR/docs/Web/CSS)|[Sequelize](https://sequelize.org/)
[HTML](https://developer.mozilla.org/pt-BR/docs/Web/HTML) | [Express](https://expressjs.com/pt-br/)
[Typescript]() | [SQLite3](https://www.sqlite.org/index.html)
[React Native](https://reactnative.dev/) | [Nodemon](https://www.npmjs.com/package/nodemon)
[]() | [Faker BR](https://www.npmjs.com/package/faker-br?activeTab=readme) 

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) and [Node](https://nodejs.org/en/) installed.

## :checkered_flag: Starting ##

```bash
# Clone the folder.
$ git clone https://gitlab.com/jpduarte/civitas-squad-vapt-vupt
    
# Access it.
$ cd civitas-squad-vapt-vupt
```

### Back-end ###

```bash
# Install the dependencies.
$ yarn install
    
# Create the .env file.
$ cp .env.example .env
    
# Generate the secret keys.
$ yarn run keys
    
# Run the migrations.
$ yarn run migrate
    
# Generate random data (optional).   
$ yarn run seed
```
    
- To test the application's routes, <a href='https://insomnia.rest/'> Insomnia </a> was used. 
Use the configuration I used through the button below:

:warning: Soon
    
### Front-end ###
    
```bash
# Install the dependencies.
$ yarn install
````

## :bulb: Roadmap ##

:warning: Soon

## :unlock: License ##

This project is under license from MIT. To learn more, see [LICENSE](LICENSE). 

## :busts_in_silhouette: Team ##

Member | Occupation Area | Graduation | University |
--- | --- | --- | --- 
<a href = "https://www.linkedin.com/in/jo%C3%A3o-pedro-de-oliveira-tavares-duarte-0a2b88150/"> João Pedro </a> | Tech Lead | Bachelor's Degree in Mathematics and Earth Science | UFRJ | 
<a href = "https://www.linkedin.com/in/livmachado"> Lívia Machado </a> | Front-end Developer| Bachelor's Degree in Mathematics and Earth Science | UFRJ | 
<a href = ""> Luiz Rodrigo </a> | Project Manager | Computer Science | UFRJ | 
<a href = "https://www.linkedin.com/in/luiza-lissandra/"> Luiza Lissandra </a> | Full Stack Developer | Electronic and Computer Engineering | UFRJ | 
<a href = "https://www.linkedin.com/in/vitor-lucio-giorgio/"> Vitor Giorgio </a> | Front-end Developer| Computer Science | UFRJ | 

&#xa0;

<hr/>

<a href="#top"> Back to top </a>