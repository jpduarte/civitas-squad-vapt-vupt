const { Router } = require('express');
const router = Router();

const passport = require("passport");
const validator = require("../config/validator");

const UserController = require("../controllers/UserController");
const SaleController = require("../controllers/SaleController");
const CommentController = require("../controllers/CommentController");
const AuthController = require("../controllers/AuthController");

const path = require("path");
const multer = require("multer");
const storage = require("../config/files");
const upload = multer({

  storage: storage,

  fileFilter: function (request, file, callback) {

    const ext = path.extname(file.originalname);

    if (ext !== ".png" && ext !== ".jpg" && ext !== ".jpeg") {

      return callback(new Error("Extensão de arquivo inválida. Tente png, jpg ou jpeg!"), false);

    };

    callback(null, true);

  },

  limits: {

    fileSize: 2048 * 2048

  }

});

// User: Auth.
router.post('/register', validator.validationUser('create'), UserController.create);
router.post('/login', AuthController.login);

router.use("/user", passport.authenticate('jwt', { session: false }));

// User: CRUD.
router.get('/user/list/all', UserController.index);
router.get('/user/list/:id', UserController.show);
router.put('/user/profile/update/:id', UserController.update);
router.delete('/user/profile/delete/:id', UserController.destroy);

// User: Add/Delete a profile photo.
router.put("/user/profile/add/photo/:id", upload.single('profile_photo'), UserController.addProfilePhoto);
router.put("/user/profile/remove/photo/:id", UserController.removeProfilePhoto);

// User: Create/Update/Delete - Sale.
router.post('/user/sale/create', validator.validationSale('create'), SaleController.create);
router.put('/user/sale/update/:id', SaleController.update);
router.delete('/user/sale/delete/:id', SaleController.destroy);

// User: Add/Delete a sale photo.
router.put("/user/sale/add/photo/:id", upload.single('banner_photo'), SaleController.addSalePhoto);
router.put("/user/sale/remove/photo/:id", SaleController.removeSalePhoto);

// User: Favorite/Disfavor - Sale.
router.put('/user/sale/add/favorite/:id', UserController.addFavoriteSale);
router.put('/user/sale/remove/favorite/:id', UserController.removeFavoriteSale);

// User: Add/Remove - Sale in cart.
router.put('/user/sale/add/cart/:id', UserController.addSaleInCart);
router.put('/user/sale/remove/cart/:id', UserController.removeSaleInCart);

// User: Create/Update/Delete - Comment. 
router.post('/user/comment/create', validator.validationComment('create'), CommentController.create);
router.put('/user/comment/update/:id', CommentController.update);
router.delete('/user/comment/delete/:id', CommentController.destroy);

// Visitor: See sales.
router.get('/sale/list/all', SaleController.index);
router.get('/sale/list/:id', SaleController.show);
router.get('/sale/services/list', SaleController.indexServices);
router.get('/sale/products/list', SaleController.indexProducts);

// Visitor: See comments.
router.get('/comment/list/all', CommentController.index);
router.get('/comment/list/:id', CommentController.show);

module.exports = router;