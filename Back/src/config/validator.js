const { body } = require("express-validator");

const validationUser = (method) => {

  switch (method) {

    case 'create': {

      return [

        body('name')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

        body('username')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

        body('email')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
          .isEmail().withMessage('Preencha com um e-mail válido, por favor.')
        ,

        body('password')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 8 }).withMessage('Por favor, preencha com no mínimo 8 caracteres.')

      ];

    };

  };

};

const validationSale = (method) => {

  switch (method) {

    case 'create': {

      return [

        body('title')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

        body('description')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

        body('type')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

      ];

    };

  };

};

const validationComment = (method) => {

  switch (method) {

    case 'create': {

      return [

        body('title')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')

      ];

    };

  };

};

module.exports = {

  validationUser,
  validationSale,
  validationComment

};