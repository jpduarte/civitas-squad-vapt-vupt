const Comment = require('../models/Comment');

const { validationResult } = require('express-validator');

// Create comment.

const create = async (request, response) => {

  try {

    validationResult(request).throw();

    const commentData = {

      title: request.body.title,
      description: request.body.description,
      date: request.body.date,
      UserId: request.body.UserId,
      SaleId: request.body.SaleId

    };

    const comment = await Comment.create(commentData);

    return response.status(201).json(comment);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List all.

const index = async (request, response) => {

  try {

    const comments = await Comment.findAll();

    return response.status(200).json({ comments });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List by Id.

const show = async (request, response) => {

  const { id } = request.params;

  try {

    const comment = await Comment.findByPk(id);

    return response.status(200).json({ comment });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Update.

const update = async (request, response) => {

  const { id } = request.params;

  try {

    const updated = await Comment.update(request.body, { where: { id: id } });

    if (updated) {

      const comment = await Comment.findByPk(id);

      return response.status(200).send(comment);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Comentário não encontrado.")

  };

};

// Delete.

const destroy = async (request, response) => {

  const { id } = request.params;

  try {

    const deleted = await Comment.destroy({ where: { id: id } });

    if (deleted) {

      return response.status(200).json("Comentário deletado com sucesso.");

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Comentário não encontrado.");

  };

};


module.exports = {

  index,
  show,
  create,
  update,
  destroy

};