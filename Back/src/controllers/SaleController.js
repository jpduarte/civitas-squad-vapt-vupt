const Sale = require('../models/Sale');

const { validationResult } = require('express-validator');

const path = require("path");
const fsPromises = require("fs").promises;

// Create sale.

const create = async (request, response) => {

  try {

    validationResult(request).throw();

    const saleData = {

      title: request.body.title,
      description: request.body.description,
      type: request.body.type,
      banner_photo: request.body.banner_photo,
      price: request.body.price,
      UserId: request.body.UserId

    };

    const sale = await Sale.create(saleData);

    return response.status(201).json({ message: "Venda cadastrada com sucesso.", sale: sale });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List all.

const index = async (request, response) => {

  try {

    const sales = await Sale.findAll({

      include: [

        'comments'

      ]

    });

    return response.status(200).json({ sales });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List only services.

const indexServices = async (request, response) => {

  try {

    const services = await Sale.findAll({

      where: {

        type: "Serviço"

      },

      include: [

        'comments'

      ]

    });

    return response.status(200).json({ services });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List only products.

const indexProducts = async (request, response) => {

  try {

    const products = await Sale.findAll({

      where: {

        type: "Produto"

      },

      include: [

        'comments'

      ]

    });

    return response.status(200).json({ products });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List by Id.

const show = async (request, response) => {

  const { id } = request.params;

  try {

    const sale = await Sale.findByPk(id, {

      include: [

        'comments'

      ]

    });

    return response.status(200).json({ sale });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Update.

const update = async (request, response) => {

  const { id } = request.params;

  try {

    const updated = await Sale.update(request.body, { where: { id: id } });

    if (updated) {

      const sale = await Sale.findByPk(id);

      return response.status(200).send(sale);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Venda não encontrada.")

  };

};

// Delete.

const destroy = async (request, response) => {

  const { id } = request.params;

  try {

    const deleted = await Sale.destroy({ where: { id: id } });

    if (deleted) {

      return response.status(200).json("Venda deletada com sucesso.");

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Venda não encontrada.");

  };

};

// Add sale photo.

const addSalePhoto = async (request, response) => {

  const { id } = request.params;

  const path = process.env.APP_URL + "/uploads/" + request.file.filename;

  try {

    const updated = await Sale.update({ banner_photo: path }, { where: { id: id } });

    if (updated) {

      const sale = await Sale.findByPk(id);

      return response.status(200).send(sale);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Venda não encontrada.")

  };

};

// Remove sale photo.

const removeSalePhoto = async (request, response) => {

  try {

    const { id } = request.params;

    const sale = await Sale.findByPk(id);

    const photo = sale.banner_photo;

    const pathDb = photo.split("/").slice(-1)[0];

    await fsPromises.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));

    await Sale.update({ banner_photo: null }, { where: { id: id } });

    const saleNew = await Sale.findByPk(id);

    return response.status(200).send(saleNew);

  } catch (error) {

    return response.status(500).json("Venda não encontrada.")

  };

};

module.exports = {

  index,
  show,
  create,
  update,
  destroy,
  indexServices,
  indexProducts,
  addSalePhoto,
  removeSalePhoto

};