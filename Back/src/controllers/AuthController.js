const UserController = require("./UserController");

const Auth = require("../config/auth");

const User = require("../models/User");

const register = async (request, response) => {

  try {

    const user = await UserController.create(request, response);

    const token = Auth.generateJWT(user);

    return response.status(200).json({token: token });

  }
  catch (error) {

    return response.status(500).json({ err: error });

  };

};

const login = async (request, response) => {

  try {

    const user = await User.findOne({ where: { username: request.body.username } });

    if (!user) {

      return response.status(404).json({ message: "Usuário não encontrado." });

    };

    const { password } = request.body;

    if (Auth.checkPassword(password, user.hash, user.salt)) {

      const token = Auth.generateJWT(user);

      return response.status(200).json({ token: token, user: user });

    } else {

      return response.status(401).json({ message: "Senha inválida." });

    };

  } catch (error) {

    return response.status(500).json({ err: error });

  };

};

module.exports = {

  register,
  login

};