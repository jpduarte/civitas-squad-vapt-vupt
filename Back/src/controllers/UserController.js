const User = require('../models/User');
const Sale = require('../models/Sale');

const { validationResult } = require('express-validator');

const Auth = require("../config/auth");

const path = require("path");
const fsPromises = require("fs").promises;

const mailer = require("../config/mail").mailer;
const handlebars = require("handlebars");
const readHTML = require("../config/mail").readHTMLFile;

// Create user.

const create = async (request, response) => {

  try {

    validationResult(request).throw();

    const  password  = request.body.password;
  
    const hashAndSalt = Auth.generatePassword(password);

    const salt = hashAndSalt.salt;
    const hash = hashAndSalt.hash;

    const listAdmins = ["luiza@gmail.com", "george@gmail.com"];

    let verificationAdmin = false;

    if (listAdmins.includes(request.body.email)) {

      verificationAdmin = true;

    };
    const userData = {

      name: request.body.name,
      email: request.body.email,
      username: request.body.username,
      address: request.body.address,
      cell_phone: request.body.cell_phone,
      profile_photo: request.body.profile_photo,
      is_admin: verificationAdmin,
      hash: hash,
      salt: salt

    };
    const user = await User.create(userData);

    // const pathTemplate = path.resolve(__dirname, "..", "..", "templates");

    // readHTML(path.join(pathTemplate, "main.html"), (err, html) => {

    //   const template = handlebars.compile(html);

    //   const replacements = {

    //     name: user.name

    //   };

    //   const htmlToSend = template(replacements);

    //   const message = {

    //     from: "testesEJCM@gmail.com",
    //     to: user.email,
    //     subject: "Bem vindo(a) à comunidade Civitas 🐰",
    //     html: htmlToSend,
    //     attachments: [{
    //       filename: 'logo.png',
    //       path: __dirname + '/../../templates/assets/logo.png',
    //       cid: 'imagelogo'
    //     }],

    //   };

    //   mailer.sendMail(message);

    // });

    return response.status(200).json({ user: user });

  } catch (err) {
    console.log(err)

    return response.status(500).json({ error: err });

  };

};

// List all.

const index = async (request, response) => {

  try {

    const users = await User.findAll({

      include: [

        'favoritesSales',
        'salesInCart',
        'sales',
        'comments'

      ]

    });

    return response.status(200).json({ users });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List by Id.

const show = async (request, response) => {

  const { id } = request.params;

  try {

    const user = await User.findByPk(id, {

      include: [

        'favoritesSales',
        'salesInCart',
        'sales',
        'comments'

      ]

    });

    return response.status(200).json({ user });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Update.

const update = async (request, response) => {

  const { id } = request.params;

  try {

    const updated = await User.update(request.body, { where: { id: id } });

    if (updated) {

      const user = await User.findByPk(id);

      return response.status(200).send(user);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

// Delete.

const destroy = async (request, response) => {

  const { id } = request.params;

  try {

    const deleted = await User.destroy({ where: { id: id } });

    if (deleted) {

      return response.status(200).json("Usuário deletado com sucesso.");

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.");

  };

};

// Favorite a sale.

const addFavoriteSale = async (request, response) => {

  const { id } = request.params;

  const { user_id } = request.query;

  try {

    let user = await User.findByPk(user_id);

    const sale = await Sale.findByPk(id);

    await user.addFavoritesSales(sale);

    user = await User.findByPk(user_id, {

      include: [

        'favoritesSales'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Disfavor a sale.

const removeFavoriteSale = async (request, response) => {

  const { id } = request.params;

  const { user_id } = request.query;

  try {

    let user = await User.findByPk(user_id);

    const sale = await Sale.findByPk(id);

    await user.removeFavoritesSales(sale);

    user = await User.findByPk(user_id, {

      include: [

        'favoritesSales'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Add a sale to cart.

const addSaleInCart = async (request, response) => {

  const { id } = request.params;

  const { user_id } = request.query;

  try {

    let user = await User.findByPk(user_id);

    const sale = await Sale.findByPk(id);

    await user.addSalesInCart(sale);

    user = await User.findByPk(user_id, {

      include: [

        'salesInCart'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Remove a sale from the cart.

const removeSaleInCart = async (request, response) => {

  const { id } = request.params;

  const { user_id } = request.query;

  try {

    let user = await User.findByPk(user_id);

    const sale = await Sale.findByPk(id);

    await user.removeSalesInCart(sale);

    user = await User.findByPk(user_id, {

      include: [

        'salesInCart'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Add profile photo.

const addProfilePhoto = async (request, response) => {

  const { id } = request.params;

  const path = process.env.APP_URL + "/uploads/" + request.file.filename;

  try {

    const updated = await User.update({ profile_photo: path }, { where: { id: id } });

    if (updated) {

      const user = await User.findByPk(id);

      return response.status(200).send(user);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

// Remove profile photo.

const removeProfilePhoto = async (request, response) => {

  try {

    const { id } = request.params;

    const user = await User.findByPk(id);

    const photo = user.profile_photo;

    const pathDb = photo.split("/").slice(-1)[0];

    await fsPromises.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));

    await User.update({ profile_photo: null }, { where: { id: id } });

    const userNew = await User.findByPk(id);

    return response.status(200).send(userNew);

  } catch (error) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

module.exports = {

  index,
  show,
  create,
  update,
  destroy,
  addFavoriteSale,
  removeFavoriteSale,
  addSaleInCart,
  removeSaleInCart,
  addProfilePhoto,
  removeProfilePhoto

};
