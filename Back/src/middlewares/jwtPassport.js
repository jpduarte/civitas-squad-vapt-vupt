const fs = require("fs");
const path = require("path");

const User = require("../models/User");

const pathToKey = path.join(__dirname, "../../", "id_rsa_pub.pem");

const PUB_KEY = fs.readFileSync(pathToKey, "utf8");

const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

const options = {

  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: PUB_KEY,
  algorithms: ['RS256']

};

module.exports = (passport) => {

  passport.use(new JWTStrategy(options, async (payload, done) => {

    await User.findByPk(payload.sub).then((user) => {

      if (user) {

        return done(null, user);

      } else {

        return done(null, false);

      };

    }).catch(err => done(err, null));

  }));

};
