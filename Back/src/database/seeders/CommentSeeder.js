const Comment = require('../../models/Comment');
const Sale = require('../../models/Sale');
const User = require('../../models/User');

const faker = require('faker-br');

const seedComment = async function () {

  try {

    await Comment.sync({ force: true });

    for (let id = 0; id < 20; id++) {

      await Comment.create({

        title: faker.name.title(),
        description: faker.lorem.sentence(),
        date: faker.date.past(),
        SaleId: faker.random.number({

          'min': 1,
          'max': 10

        }),
        UserId: faker.random.number({

          'min': 1,
          'max': 10

        })

      });

      let user = await User.findByPk(

        faker.random.number({

          'min': 1,
          'max': 10

        })

      );

      let sale = await Sale.findByPk(

        faker.random.number({

          'min': 1,
          'max': 15

        })

      );

      user.addFavoritesSales(sale);

      sale = await Sale.findByPk(

        faker.random.number({

          'min': 1,
          'max': 15

        })

      );

      user.addSalesInCart(sale);

    };

  } catch (err) {

    console.log(err);

  };

};

module.exports = seedComment;