const Users = require('../../models/User');

const faker = require('faker-br');

const seedUser = async function () {

  try {

    await Users.sync({ force: true });

    for (let id = 0; id < 10; id++) {

      await Users.create({

        name: faker.name.firstName(),
        email: faker.internet.email(),
        username: faker.internet.userName(),
        hash: faker.random.uuid(),
        salt: faker.random.uuid(),
        cell_phone: faker.phone.phoneNumber(),
        address: faker.address.streetAddress(),
        profile_photo: faker.image.imageUrl(),
        is_admin: faker.random.boolean()

      });

    };

  } catch (err) {

    console.log(err);

  };

};

module.exports = seedUser;