const Sale = require('../../models/Sale');
const User = require('../../models/User');

const faker = require('faker-br');

const seedSale = async function () {

  try {

    await Sale.sync({ force: true });

    for (let id = 0; id < 15; id++) {

      await Sale.create({

        title: faker.name.title(),
        description: faker.lorem.sentence(),
        type: (id % 2 == 0 ? "Produto" : "Serviço"),
        photo_banner: faker.image.imageUrl(),
        price: faker.commerce.price(),
        UserId: faker.random.number({

          'min': 1,
          'max': 10

        })

      });

      // if (id != 0) {

      //   let user = await User.findByPk(

      //     faker.random.number({

      //       'min': 1,
      //       'max': 10

      //     })

      //   );

      //   let sale = await Sale.findByPk(

      //     faker.random.number({

      //       'min': 1,
      //       'max': id

      //     })

      //   );

      //   user.addFavoritesSales(sale);

      //   let sale = await Sale.findByPk(

      //     faker.random.number({

      //       'min': 1,
      //       'max': id

      //     })

      //   );

      //   user.addSalesInCart(sale);

      // };

    };

  } catch (err) {

    console.log(err);

  };

};

module.exports = seedSale;