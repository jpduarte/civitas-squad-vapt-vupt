require('../../config/dotenv')();
require('../../config/sequelize');

const seedUser = require('./UserSeeder');
const seedSale = require('./SaleSeeder');
const seedComment = require('./CommentSeeder');

(async () => {

  try {

    await seedUser();
    await seedSale();
    await seedComment();

  } catch (err) {

    console.log(err);

  }

})();
