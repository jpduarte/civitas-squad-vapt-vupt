const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Comment = sequelize.define('Comment', {

  title: {

    type: DataTypes.STRING,
    allowNull: false

  },

  description: {

    type: DataTypes.STRING

  },

  date: {

    type: DataTypes.DATEONLY

  }

},

  { timestamps: false }

);

Comment.associate = function (models) {

  Comment.belongsTo(models.User);
  Comment.belongsTo(models.Sale);

};

module.exports = Comment;