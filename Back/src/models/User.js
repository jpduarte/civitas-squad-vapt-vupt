const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {

  name: {

    type: DataTypes.STRING,
    allowNull: false

  },

  email: {

    type: DataTypes.STRING,
    unique: true,
    allowNull: false

  },

  username: {

    type: DataTypes.STRING,
    unique: true,
    allowNull: false

  },

  hash: {

    type: DataTypes.STRING,

  },

  salt: {

    type: DataTypes.STRING,

  },

  cell_phone: {

    type: DataTypes.STRING,

  },

  address: {

    type: DataTypes.STRING,

  },

  profile_photo: {

    type: DataTypes.STRING,

  },

  is_admin: {

    type: DataTypes.BOOLEAN

  }

},

  { timestamps: false }

);

User.associate = function (models) {

  User.hasMany(models.Sale, {

    as: 'sales'

  });

  User.hasMany(models.Comment, {

    as: 'comments'

  });

  User.belongsToMany(models.Sale, {

    through: 'favoriteSalesList',
    as: 'favoritesSales',
    foreignKey: 'userTannerId'

  });

  User.belongsToMany(models.Sale, {

    through: 'salesListInCart',
    as: 'salesInCart',
    foreignKey: 'userBuyerId'

  });

};

module.exports = User;