const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Sale = sequelize.define('Sale', {

  title: {

    type: DataTypes.STRING,
    allowNull: false

  },

  description: {

    type: DataTypes.STRING,
    allowNull: false

  },

  type: {

    type: DataTypes.STRING,
    allowNull: false

  },

  banner_photo: {

    type: DataTypes.STRING

  },

  price: {

    type: DataTypes.STRING,
    allowNull: false

  },

},

  { timestamps: false }

);

Sale.associate = function (models) {

  Sale.belongsTo(models.User);

  Sale.hasMany(models.Comment, {

    as: 'comments'

  });

  Sale.belongsToMany(models.User, {

    through: 'favoriteSalesList',
    as: 'tanners',
    foreignKey: 'saleFavoriteId'

  });

  Sale.belongsToMany(models.User, {

    through: 'salesListInCart',
    as: 'buyers',
    foreignKey: 'saleInCartId'

  });

};

module.exports = Sale;